Source: python-bx
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Michael R. Crusoe <crusoe@debian.org>
Section: python
Testsuite: autopkgtest-pkg-pybuild
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               zlib1g-dev,
               python3-setuptools,
               pybuild-plugin-pyproject,
               python3-all-dev:any,
               python3-lzo,
               python3-pyparsing <!nocheck>,
               python3-pytest <!nocheck>,
               python3-pytest-cython <!nocheck>,
               python3-numpy <!nocheck>,
               cython3
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/python-bx
Vcs-Git: https://salsa.debian.org/med-team/python-bx.git
Homepage: https://github.com/bxlab/bx-python
Rules-Requires-Root: no

Package: python3-bx
Architecture: any
Depends: python3-lzo,
         ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends}
Provides: python3-bx-tools
Description: library to manage genomic data and its alignment
 The bx-python project is a Python3 library and associated set of scripts to
 allow for rapid implementation of genome scale analyses. The library contains
 a variety of useful modules, but the particular strengths are:
  * Classes for reading and working with genome-scale multiple local
    alignments (in MAF, AXT, and LAV formats)
  * Generic data structure for indexing on disk files that contain blocks of
    data associated with intervals on various sequences (used, for example, to
    provide random access to individual alignments in huge files; optimized
    for use over network filesystems)
  * Data structures for working with intervals on sequences
  * "Binned bitsets" which act just like chromosome sized bit arrays, but
    lazily allocate regions and allow large blocks of all set or all unset
    bits to be stored compactly
  * "Intersecter" for performing fast intersection tests that preserve both
    query and target intervals and associated annotation
